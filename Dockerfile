ARG NSO_IMAGE_PATH
ARG NSO_VERSION
ARG PKG_PATH

# compile NED packages, local packages etc
FROM ${NSO_IMAGE_PATH}cisco-nso-dev:${NSO_VERSION} AS build

# copy NED packages from outside the container into the build container
COPY /ned-packages /ned-packages

# run the NED packages - they are self-extracting files and output the .tar.gz
# of the NED together with files to verify the signature of the NEDs tar.gz file
RUN cd /ned-packages; for NED_FILE in $(ls /ned-packages); do sh /ned-packages/${NED_FILE}; done
# extract the tar balls
RUN mkdir -p /packages; cd /packages; for NED_TARBALL in $(ls /ned-packages/*.tar.gz); do tar zxf /ned-packages/$(basename ${NED_TARBALL}); done

# Compile the packages
# This isn't strictly necessary as the NEDs are shipped in a compiled form but
# they are compiled for a specific version. The recommendation is that the NEDs
# should be compiled with the major/minor version of NSO you are using,
# preferably the same maintenance version as well. Switching to a new NSO
# version thus entails downloading other packages of the NEDs even if the
# version of the NED is the same. By compiling them ourselves we gain freedom -
# we can now freely switch NSO version without downloading new NEW packages
# (this is of course only true to a certain extent but most NEDs are compatible
# across a rather wide range of NSO versions).
#
# We don't care for compiling netsims here - for that, look at the disaggregated
# repo structure where NEDs are kept on their own repositories.
RUN for NED in $(ls /packages); do make -C /packages/${NED}/src; done

# build the final production NSO image
FROM ${NSO_IMAGE_PATH}cisco-nso-base:${NSO_VERSION} AS nso

# copy in local package
COPY --from=build /packages /var/opt/ncs/packages/
